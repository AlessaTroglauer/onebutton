using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    private Rigidbody2D rb2d;
    private BoxCollider2D boxCollider2D;
    [SerializeField] private LayerMask platformLayerMask; 

    private void Start()
    {
        rb2d = transform.GetComponent<Rigidbody2D>();
        boxCollider2D = transform.GetComponent<BoxCollider2D>();
        
    }

    private void Update()
    {
        
        
        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space)  )
        {
            float jumpVelocity = 7f; 
            rb2d.velocity = Vector2.up * jumpVelocity; 
        }

        //Different jump height depending on button press
        if (Input.GetButtonUp("Jump") && rb2d.velocity.y > 0)
        {
            rb2d.velocity = rb2d.velocity * 0.5f;
        }
     
    }

    private void FixedUpdate()
    {
        rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y) ;
    }

    private bool IsGrounded()
    {
        RaycastHit2D raycastHit2D = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, .3f, platformLayerMask);
        Debug.Log(raycastHit2D.collider); 
        return raycastHit2D.collider != null; 
    }
}
